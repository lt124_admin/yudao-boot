package cn.iocoder.yudao.module.system.controller.admin.db.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Schema(description = "管理后台 - sql语句 Request VO")
@Data
public class DbSqlReqVO extends DbConnReqVO {

    @Schema(description = "sql语句", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank(message = "sql语句不能为空")
    private String sql;
}

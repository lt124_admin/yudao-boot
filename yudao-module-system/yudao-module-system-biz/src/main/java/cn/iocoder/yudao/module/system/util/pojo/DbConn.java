package cn.iocoder.yudao.module.system.util.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

/**
 * 数据库连接
 *
 * @author liutao
 * @version 1.0
 * @since 1.8
 * @since 2024/11/7 22:46
 */
@Data
@AllArgsConstructor
public class DbConn implements Serializable {
    private String dbUrl;
    private String dbPort = "3306";
    private String dbName = "mysql";
    private String dbUser = "root";
    private String dbPwd;
    private String dbProp = "useSSL=false&serverTimezone=Asia/Shanghai&allowPublicKeyRetrieval=true&nullCatalogMeansCurrent=true&rewriteBatchedStatements=true";
}

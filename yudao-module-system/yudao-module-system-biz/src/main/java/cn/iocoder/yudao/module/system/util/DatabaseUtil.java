package cn.iocoder.yudao.module.system.util;

import cn.iocoder.yudao.module.system.util.pojo.DbConn;
import cn.iocoder.yudao.module.system.util.pojo.TableColumn;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * 数据库工具类
 *
 * @author liutao
 * @version 1.0
 * @since 1.8
 * @since 2024/11/5 21:26
 */
public class DatabaseUtil {

    /**
     * 获取数据库连接
     *
     * @param dbConn 数据库连接信息
     *
     * @return 数据库连接
     */
    public static Connection getConnection(DbConn dbConn){
        Connection conn = null;
        try {
            String url = String.format("jdbc:mysql://%s:%s/%s?%s", dbConn.getDbUrl(), dbConn.getDbPort(), dbConn.getDbName(), dbConn.getDbProp());
            conn = DriverManager.getConnection(url, dbConn.getDbUser(), dbConn.getDbPwd());
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }

    /**
     * 获取权限内所有数据库
     *
     * @param conn 数据库连接
     *
     * @return 所有数据库名称
     */
    public static List<String> getAllDbNames(Connection conn) {
        List<String> databaseNames = new ArrayList<>();

        try {
            // 3. 执行查询
            DatabaseMetaData metaData = conn.getMetaData();
            ResultSet databases = metaData.getCatalogs();

            // 4. 处理结果集
            while (databases.next()) {
                String dbName = databases.getString(1);
                System.out.println("Database: " + dbName);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return databaseNames;
    }

    /**
     * 获取权限内所有表
     *
     * @param conn 数据库连接
     *
     * @return 所有表名称
     */
    public static List<String> getAllTables(Connection conn) {
        List<String> tableNames = new ArrayList<>();

        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("SHOW TABLES")) {

            while (rs.next()) {
                tableNames.add(rs.getString(1));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return tableNames;
    }

    /**
     * 检测表是否存在
     * @param conn  数据库连接
     * @param tableName 表名称
     * @return
     * @throws SQLException
     */
    public static boolean checkTableExists(Connection conn, String tableName) {
        boolean exists;
        ResultSet tables = null;
        try {
            DatabaseMetaData metaData = conn.getMetaData();
            tables = metaData.getTables(null, null, tableName.toUpperCase(), new String[]{"TABLE"});
            exists = tables.next();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            // 确保在finally块中关闭ResultSet和可能的其他资源
            try {
                if (tables != null) {
                    tables.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return exists;
    }

    /**
     * 获取表结构
     *
     * @param conn 数据库连接
     *
     * @param tableName 表名
     * @return 表结构
     */
    public static List<TableColumn> getTableStructure(Connection conn, String dbName, String tableName) {
        List<TableColumn> TableColumns = new ArrayList<>();
        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT COLUMN_NAME, DATA_TYPE, COLUMN_TYPE, COLUMN_DEFAULT, IS_NULLABLE, COLUMN_KEY, EXTRA, COLUMN_COMMENT FROM COLUMNS WHERE TABLE_SCHEMA='"+dbName+"' AND TABLE_NAME='"+tableName+"'")) {
            while (rs.next()) {
                //列的名称
                String columnName = rs.getString("COLUMN_NAME");
                //列的数据类型
                String dataType = rs.getString("DATA_TYPE");
                //列的数据类型
                String columnType = rs.getString("COLUMN_TYPE");
                //列的数据类型
                String columnDefault = rs.getString("COLUMN_DEFAULT");
                //指示该列是否允许 NULL 值
                String columnNullable = rs.getString("IS_NULLABLE");
                //该列是否被索引为键的一部分
                String columnKey = rs.getString("COLUMN_KEY");
                //任何额外的列信息（例如 auto_increment）
                String columnExtra = rs.getString("Extra");
                //列的注释
                String columnComment = rs.getString("COLUMN_COMMENT");

                TableColumn column = new TableColumn(columnName, dataType, columnType, columnDefault, columnNullable, columnKey, columnExtra, columnComment);
                TableColumns.add(column);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return TableColumns;
    }

    /**
     * 执行DDL语句，如创建表、索引等
     *
     * @param conn 数据库连接
     *
     * @param sql    DDL语句
     *
     */
    public synchronized static void executeDDL(Connection conn, String sql) {
        try (Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 执行DML语句，如插入、更新、删除等
     *
     * @param conn 数据库连接
     * @param sql    DML语句
     * @param params 参数列表
     */
    public synchronized static void executeDML(Connection conn, String sql, Object... params) {
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    pstmt.setObject(i + 1, params[i]);
                }
            }
            pstmt.executeUpdate();
            System.out.println("DML executed successfully: " + sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * 执行查询语句，并返回结果集
     *
     * @param conn 数据库连接
     * @param sql    查询语句
     * @param resultSetConsumer 结果集处理器，用于处理结果集
     * @param params 参数列表
     */
    public synchronized static void executeQuery(Connection conn, String sql, Consumer<ResultSet> resultSetConsumer, Object... params) {
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    pstmt.setObject(i + 1, params[i]);
                }
            }
            try (ResultSet rs = pstmt.executeQuery()) {
                resultSetConsumer.accept(rs);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        /*
        DatabaseUtil.createDatabase("aaa");
        // 示例DDL操作
        String createTableSQL = "CREATE TABLE IF NOT EXISTS aaa.test_table (id INT PRIMARY KEY, name VARCHAR(50))";
        DatabaseUtil.executeDDL(createTableSQL);

        // 示例DML操作
        String insertSQL = "INSERT INTO aaa.test_table (id, name) VALUES (?, ?)";
        DatabaseUtil.executeDML(insertSQL, 1, "John Doe");

        // 示例查询操作
        String selectSQL = "SELECT * FROM aaa.test_table";
        DatabaseUtil.executeQuery(selectSQL, rs -> {
            try {
                while (rs.next()) {
                    System.out.println("ID: " + rs.getInt("id") + ", Name: " + rs.getString("name"));
                }
                DatabaseUtil.closeResultSet(rs);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });

        List<String> tables = DatabaseUtil.getAllTablesAndStructures();
        tables.forEach(System.out::println);
        DatabaseUtil.closeConnection(DatabaseUtil.getConnection());
        */
    }
}

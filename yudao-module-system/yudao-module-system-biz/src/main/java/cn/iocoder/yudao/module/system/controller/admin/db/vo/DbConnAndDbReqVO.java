package cn.iocoder.yudao.module.system.controller.admin.db.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Schema(description = "管理后台 - 数据源连接 Request VO")
@Data
public class DbConnAndDbReqVO extends DbConnReqVO{

    @Schema(description = "新数据库名", requiredMode = Schema.RequiredMode.REQUIRED, example = "mysql")
    @NotBlank(message = "新数据库名不能为空")
    private String newDbName;

}

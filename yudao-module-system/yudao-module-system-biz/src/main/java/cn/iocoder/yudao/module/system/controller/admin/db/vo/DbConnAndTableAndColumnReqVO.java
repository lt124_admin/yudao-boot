package cn.iocoder.yudao.module.system.controller.admin.db.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.util.List;

@Schema(description = "管理后台 - 数据源连接、新表名称与所有列 Request VO")
@Data
public class DbConnAndTableAndColumnReqVO extends DbConnAndTableReqVO {

    @Schema(description = "表名称", requiredMode = Schema.RequiredMode.REQUIRED, example = "test")
    @NotBlank(message = "表名称不能为空")
    private String tableName;

    @Schema(description = "数据库表结构", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank(message = "数据库表结构不能为空")
    private List<TableColumnReqVO> tableColumns;

}

package cn.iocoder.yudao.module.system.controller.admin.db.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.NumberFormat;

import java.io.Serializable;

@Schema(description = "管理后台 - 数据源连接 Request VO")
@Data
public class DbConnReqVO implements Serializable {

    @Schema(description = "数据库地址", requiredMode = Schema.RequiredMode.REQUIRED, example = "127.0.0.1")
    @NotBlank(message = "数据库地址不能为空")
    private String dbUrl;

    @Schema(description = "数据库地址", example = "3306")
    @NumberFormat
    private String dbPort;

    @Schema(description = "数据库用户", requiredMode = Schema.RequiredMode.REQUIRED, example = "root")
    private String dbUser;

    @Schema(description = "数据库密码", requiredMode = Schema.RequiredMode.REQUIRED, example = "root")
    @NotNull(message = "数据库密码不能为空")
    private Integer dbPwd;

    @Schema(description = "数据库名", requiredMode = Schema.RequiredMode.REQUIRED, example = "mysql")
    private String dbName;

}

package cn.iocoder.yudao.module.system.util.pojo;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Schema(description="单个列的信息")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TableColumn implements Serializable {
    private String columnName;
    private String dataType;
    private String columnType;
    private String columnDefault;
    private String columnNullable;
    private String columnKey;
    private String columnExtra;
    private String columnComment;
}
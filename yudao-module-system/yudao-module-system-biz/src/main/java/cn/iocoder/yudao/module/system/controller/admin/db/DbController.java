package cn.iocoder.yudao.module.system.controller.admin.db;

import cn.iocoder.yudao.framework.common.pojo.CommonResult;
import cn.iocoder.yudao.framework.common.util.object.BeanUtils;
import cn.iocoder.yudao.module.system.controller.admin.db.vo.DbConnAndDbReqVO;
import cn.iocoder.yudao.module.system.controller.admin.db.vo.DbConnAndTableReqVO;
import cn.iocoder.yudao.module.system.controller.admin.db.vo.DbConnReqVO;
import cn.iocoder.yudao.module.system.util.DatabaseUtil;
import cn.iocoder.yudao.module.system.util.pojo.DbConn;
import cn.iocoder.yudao.module.system.util.pojo.TableColumn;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static cn.iocoder.yudao.framework.common.pojo.CommonResult.success;

@Tag(name = "管理后台 - 数据库管理")
@RestController
@RequestMapping("/system/db")
@Validated
public class DbController {

    @PostMapping("/getDbNames")
    @Operation(summary = "设置数据库连接")
    @PreAuthorize("@ss.hasPermission('system:db:conn')")
    public CommonResult<List<String>> getDbNames(@Valid @RequestBody DbConnReqVO dbConnReqVO) {
        List<String> allDbNames = new ArrayList<>();
        DbConn dbConn = BeanUtils.toBean(dbConnReqVO, DbConn.class);
        try (Connection conn = DatabaseUtil.getConnection(dbConn);) {
            allDbNames = DatabaseUtil.getAllDbNames(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success(allDbNames);
    }

    @PostMapping("/createDb")
    @Operation(summary = "创建数据库")
    @PreAuthorize("@ss.hasPermission('system:db:createDb')")
    public CommonResult<String> createDb(@Valid @RequestBody DbConnAndDbReqVO dbConnAndDbReqVO) {
        DbConn dbConn = BeanUtils.toBean(dbConnAndDbReqVO, DbConn.class);
        try (Connection conn = DatabaseUtil.getConnection(dbConn);) {
            String sql = String.format("CREATE DATABASE %s DEFAULT CHARACTER SET utf8mb4 DEFAULT COLLATE utf8mb4_general_ci", dbConnAndDbReqVO.getNewDbName());
            DatabaseUtil.executeDDL(conn, sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success("创建数据库成功！");
    }

    @PostMapping("/dropDb")
    @Operation(summary = "删除数据库")
    @PreAuthorize("@ss.hasPermission('system:db:dropDb')")
    public CommonResult<String> dropDb(@Valid @RequestBody DbConnAndDbReqVO dbConnAndDbReqVO) {
        DbConn dbConn = BeanUtils.toBean(dbConnAndDbReqVO, DbConn.class);
        try (Connection conn = DatabaseUtil.getConnection(dbConn);) {
            String sql = String.format("DROP DATABASE %s", dbConnAndDbReqVO.getNewDbName());
            DatabaseUtil.executeDDL(conn, sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success("删除数据库成功！");
    }

    @PostMapping("/getAllTables")
    @Operation(summary = "获取数据库所有表")
    @PreAuthorize("@ss.hasPermission('system:db:getAllTables')")
    public CommonResult<List<String>> getAllTables(@Valid @RequestBody DbConnReqVO dbConnReqVO) {
        List<String> allTables = new ArrayList<>();
        DbConn dbConn = BeanUtils.toBean(dbConnReqVO, DbConn.class);
        try (Connection conn = DatabaseUtil.getConnection(dbConn);) {
            allTables = DatabaseUtil.getAllTables(conn);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success(allTables);
    }

    @PostMapping("/createTable")
    @Operation(summary = "创建数据库表")
    @PreAuthorize("@ss.hasPermission('system:db:createTable')")
    public CommonResult<String> createTable(@Valid @RequestBody DbConnAndDbReqVO dbConnAndDbReqVO) {
        DbConn dbConn = BeanUtils.toBean(dbConnAndDbReqVO, DbConn.class);
        try (Connection conn = DatabaseUtil.getConnection(dbConn);) {
            String sql = String.format("DROP DATABASE %s", dbConnAndDbReqVO.getNewDbName());
            DatabaseUtil.executeDDL(conn, sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success("删除数据库成功！");
    }

    @PostMapping("/getTableStructure")
    @Operation(summary = "获取表结构")
    @PreAuthorize("@ss.hasPermission('system:db:getTableStructure')")
    public CommonResult<List<TableColumn>> getTableStructure(@Valid @RequestBody DbConnAndTableReqVO dbConnAndTableReqVO) {
        List<TableColumn> tableColumns = new ArrayList<>();
        DbConn dbConn = BeanUtils.toBean(dbConnAndTableReqVO, DbConn.class);
        try (Connection conn = DatabaseUtil.getConnection(dbConn);) {
            tableColumns = DatabaseUtil.getTableStructure(conn, dbConnAndTableReqVO.getDbName(), dbConnAndTableReqVO.getTableName());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return success(tableColumns);
    }


}

package cn.iocoder.yudao.module.system.controller.admin.db.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.io.Serializable;

@Schema(description = "管理后台 - 数据列设计 Request VO")
@Data
public class TableColumnReqVO implements Serializable {

    @Schema(description = "列名称", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank(message = "列名称不能为空")
    private String columnName;

    @Schema(description = "列类型", requiredMode = Schema.RequiredMode.REQUIRED)
    @NotBlank(message = "列类型不能为空")
    private String columnType;

    @Schema(description = "列长度")
    private String columnLength;

    @Schema(description = "是否允许控制")
    private String columnNullable;

    @Schema(description = "列主键")
    private String columnKey;

    @Schema(description = "列注释")
    @NotBlank(message = "列注释不能为空")
    private String columnComment;
}
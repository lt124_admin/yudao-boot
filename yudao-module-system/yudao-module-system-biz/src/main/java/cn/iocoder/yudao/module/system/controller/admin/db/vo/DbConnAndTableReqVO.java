package cn.iocoder.yudao.module.system.controller.admin.db.vo;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Schema(description = "管理后台 - 数据源连接与表 Request VO")
@Data
public class DbConnAndTableReqVO extends DbConnReqVO{

    @Schema(description = "表名称", requiredMode = Schema.RequiredMode.REQUIRED, example = "test")
    @NotBlank(message = "表名称不能为空")
    private String tableName;

}

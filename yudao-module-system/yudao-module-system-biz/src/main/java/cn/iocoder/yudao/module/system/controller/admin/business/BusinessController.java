package cn.iocoder.yudao.module.system.controller.admin.business;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.core.util.StrUtil;
import cn.iocoder.yudao.framework.common.pojo.CommonResult;
import cn.iocoder.yudao.module.infra.api.config.ConfigApi;
import cn.iocoder.yudao.module.system.util.DatabaseUtil;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import jakarta.annotation.security.PermitAll;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.sql.DataSource;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static cn.iocoder.yudao.framework.common.pojo.CommonResult.error;
import static cn.iocoder.yudao.framework.common.pojo.CommonResult.success;

@Tag(name = "管理后台 - 业务系统生成")
@RestController("businessController")
@RequestMapping("/system/business")
@Log4j2
public class BusinessController {

    @Resource
    private DataSource dataSource;
    @Resource
    private ConfigApi configApi;

    @PermitAll
    @PostMapping("/sql")
    public CommonResult<List<String>> uploadSqlFile(@RequestPart("file") MultipartFile file) throws SQLException, IOException {
        if (file.isEmpty()) {
            return error(-1, "请上传一个sql文件");
        }

        // 获取文件名并生成一个唯一的文件名以避免冲突
        String originalFilename = file.getOriginalFilename();
        // 检查文件扩展名是否为 .sql
        if (!originalFilename.toLowerCase().endsWith(".sql")) {
            return error(-2, "请上传一个以 .sql 结尾的文件");
        }
        String destPath = configApi.getConfigValueByKey("codegen.path") + File.separator + "archive" + File.separator + "sql";
        // 确保目标目录存在
        FileUtil.mkdir(destPath);

        try (InputStream inputStream = file.getInputStream()) {
            // 创建目标文件对象
            File destFile = new File(destPath + File.separator + originalFilename);
            // 使用FileUtil.writeFromStream方法将输入流写入到目标文件
            FileUtil.writeFromStream(inputStream, destFile);
        }

        List<String> sqlScript =  new ArrayList<>();
        try (InputStream inputStream = file.getInputStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {

            String line;
            while ((line = reader.readLine()) != null) {
                // 处理每一行数据
                sqlScript.add(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        List<String> tableNames = new ArrayList<>();
        createTable(tableNames, sqlScript);
        return success(tableNames);
    }

    private void createTable(List<String> tableNames, List<String> sqlScript) throws SQLException {
        StringBuilder statement = new StringBuilder();

        // 定义正则表达式来匹配表名
        String regex = "CREATE TABLE\\s+([\\w_]+)";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        String tableName = "";
        for (String line : sqlScript) {
            // 去除行尾的空白字符，包括换行符
            String trimmedLine = line.trim();

            // 如果当前行是空行或者是一个注释行（以--开头），则跳过它
            if (trimmedLine.isEmpty() || trimmedLine.startsWith("--")) {
                continue;
            }

            if (trimmedLine.contains("CREATE TABLE")) {
                Matcher matcher = pattern.matcher(trimmedLine);
                if (matcher.find()) {
                    tableName = matcher.group(1);
                    tableNames.add(tableName);
                }
            }

            // 将当前行添加到当前正在构建的语句中
            statement.append(trimmedLine).append(" ");

            // 检查是否遇到了语句的结束（分号）
            if (trimmedLine.endsWith(";")) {
                // 去除尾部的分号和额外的空格
                String fullStatement = statement.toString().trim().replaceAll(";\\s*$", "");

                // 检查是否是CREATE TABLE语句
                if (fullStatement.startsWith("CREATE TABLE")) {
                    Connection conn = dataSource.getConnection();
                    boolean exist = DatabaseUtil.checkTableExists(dataSource.getConnection(), tableName);
                    if(exist){
                        DatabaseUtil.executeDDL(conn,"DROP TABLE "+tableName);
                    }
                    DatabaseUtil.executeDDL(conn,fullStatement);
                    conn.close();
                }
                // 重置当前正在构建的语句
                statement.setLength(0);
            }
        }

    }

    @PermitAll
    @GetMapping("/initDb")
    public CommonResult<List<String>> initDb() throws InterruptedException {
        String codegenPath = configApi.getConfigValueByKey("codegen.path");
        List<String> logs = new ArrayList<>();
        logs.add("停止已经运行的yudao-boot-mysql容器");
        RuntimeUtil.exec("docker stop yudao-boot-mysql");
        logs.add("删除yudao-boot-mysql容器");
        RuntimeUtil.exec("docker rm -f yudao-boot-mysql");
        //将芋道数据库的初始化sql复制到/tmp/codegen/archive/sql/目录下，方便mysql初始化
        logs.add("解压芋道数据库的初始化sql到/tmp/codegen/archive/目录下");
        RuntimeUtil.exec("rm -rf " + codegenPath + "archive/mysql");
        RuntimeUtil.exec("tar zxf " + codegenPath + "archive/mysql.tar.gz -C " + codegenPath + "archive/");
        try {
            Thread.sleep(1000*5);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        logs.add("启动yudao-boot-mysql容器");
        RuntimeUtil.exec("docker run -d -p 23306:3306 -e MYSQL_ROOT_USER=yudao-boot -e MYSQL_ROOT_PASSWORD=yudao-boot -e MYSQL_DATABASE=yudao-boot -v " + codegenPath + "archive/mysql/data:/bitnami/mysql/data -v " + codegenPath + "archive/sql/:/docker-entrypoint-initdb.d/ --name yudao-boot-mysql bitnami/mysql:8.0.40");

        logs.add("停止已经运行的yudao-boot-redis容器");
        RuntimeUtil.exec("docker stop yudao-boot-redis");
        logs.add("删除yudao-boot-redis容器");
        RuntimeUtil.exec("docker rm -f yudao-boot-redis");
        logs.add("启动yudao-boot-redis容器");
        RuntimeUtil.exec("sysctl vm.overcommit_memory=1");
        RuntimeUtil.exec("sysctl -w net.core.somaxconn=512");
        try {
            Thread.sleep(1000*5);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        RuntimeUtil.exec("docker run -d -p 26379:6379 --name yudao-boot-redis redis:7");
        logs.add("数据正在初始化，预计需要60秒，请稍等...");
        return success(logs);
    }

    @PermitAll
    @GetMapping("/initDbLogs")
    public CommonResult<List<String>> initDbLogs(){
        List<String> logs = new ArrayList<>();
        logs.add("yudao-boot-mysql服务的日志如下：");
        logs.addAll(RuntimeUtil.execForLines("docker logs -n 100 yudao-boot-mysql"));
        logs.add("yudao-boot-redis服务的日志如下：");
        logs.addAll(RuntimeUtil.execForLines("docker logs -n 100 yudao-boot-redis"));
        logs.add("数据初始化完成");
        return success(logs);
    }

    @PermitAll
    @GetMapping("/compile")
    public CommonResult<List<String>> compile() {
        String codegenPath = configApi.getConfigValueByKey("codegen.path");
        String serverIp = configApi.getConfigValueByKey("server.ip");
        String compileExec = "";
        if(StrUtil.isBlank(serverIp)){
            compileExec = "docker run -d -v /var/run/docker.sock:/var/run/docker.sock -v /root/.m2:/root/.m2 -v " + codegenPath + "archive:/tmp/codegen/archive --name yudao-compile yudao-compile:1.0";
        }else {
            compileExec = "docker run -d -v /var/run/docker.sock:/var/run/docker.sock -v /root/.m2:/root/.m2 -v " + codegenPath + "archive:/tmp/codegen/archive -e SERVER_IP=" + serverIp + " --name yudao-compile yudao-compile:1.0";
        }
        List<String> logs = new ArrayList<>();
        logs.add("停止已经运行的yudao-compile容器");
        RuntimeUtil.exec("docker stop yudao-compile");
        logs.add("删除yudao-compile容器");
        RuntimeUtil.exec("docker rm -f yudao-compile");
        try {
            Thread.sleep(1000*10);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        logs.add("启动程序编译镜像");
        log.info("serverIp:{}",serverIp);
        log.info("codegenPath:{}",codegenPath);
        log.info("运行的容器命令：{}",compileExec);
        String execLog = RuntimeUtil.execForStr(compileExec);
        log.info("execLog:{}",execLog);
        logs.add("项目正在编译中，预计需要5-10分钟，请等待...");
        return success(logs);
    }

    @PermitAll
    @GetMapping("/compileLogs")
    public CommonResult<List<String>> compileLogs() {
        List<String> logs = new ArrayList<>();
        logs.add("编译日志如下：");
        logs.addAll(RuntimeUtil.execForLines("docker logs -n 100 yudao-compile"));
        return success(logs);
    }

    @PermitAll
    @GetMapping("/startApp")
    public CommonResult<List<String>> startApp() {
        String codegenPath = configApi.getConfigValueByKey("codegen.path");
        List<String> logs = new ArrayList<>();
        logs.add("启动程序，日志如下：");
        try {
            logs.addAll(RuntimeUtil.execForLines("sh " + codegenPath+"archive/start.sh"));
        }catch (Exception e) {
            logs.add("程序启动失败，请查看日志");
        }

        return success(logs);
    }
}

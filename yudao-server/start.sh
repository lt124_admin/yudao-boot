#!/bin/sh
# 服务端容器名称
CONTAINER_SERVER_NAME="yudao-boot"
echo "删除 $CONTAINER_SERVER_NAME 服务"
docker rm -f  "$CONTAINER_SERVER_NAME"
sleep 1s
echo "正在运行新容器 $CONTAINER_SERVER_NAME..."
# 使用 docker run 命令启动新的容器
docker run -d -p 28080:48080 --name $CONTAINER_SERVER_NAME $CONTAINER_SERVER_NAME:runtime

# 前端容器名称
CONTAINER_UI_NAME="yudao-ui"
echo "删除 $CONTAINER_UI_NAME 服务"
docker rm -f "$CONTAINER_UI_NAME"
sleep 1s
echo "正在运行新容器 $CONTAINER_UI_NAME..."
# 使用 docker run 命令启动新的容器
docker run -d -p 28088:80 --name "$CONTAINER_UI_NAME" $CONTAINER_UI_NAME:runtime

echo "恭喜你，芋道部署成功！！！"
exit